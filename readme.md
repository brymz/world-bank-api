# WORLD BANK API

This is a REST Application based on Springboot that performs request to [The World Bank](https://data.worldbank.org/) in order to calculate
some custom indicators such as "Population Growth by Country" and "GDP/PPP Growth by Country".

## Requirements
- Java JDK 8 (or later).
- Maven 3.6 (or later).
- Internet Connection Access.

## Setup
1.- Clone [git repository](https://bitbucket.org/brymz/ra_metrics.git) from bitbucket.  
```sh
$ git clone https://brymz@bitbucket.org/brymz/world-bank-api.git
```
2.- Build project by using Maven.
```sh
$ cd world-bank-api/
$ mvn clean install
```
3.- Execute as Java JAR application
```sh
$ java -jar target/worldbankapi-1.0.0.jar
```  
OR as Spring-boot Application
```sh
$ mvn spring-boot:run
```  
4.- Go to browser and open application at:
```sh
http://localhost:8080/world-bank-api/
```  
## Author
Ing. [Brayam Rivas](mailto:ing.brayamrivas@gmail.com)

## License
[MIT License](https://choosealicense.com/licenses/mit/)

Copyright (c) 2020 Brayam Rivas

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
