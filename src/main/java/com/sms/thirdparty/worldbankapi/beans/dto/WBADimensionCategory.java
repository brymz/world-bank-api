package com.sms.thirdparty.worldbankapi.beans.dto;

import lombok.Data;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

@Data
public class WBADimensionCategory {

    private static final Logger LOG = LoggerFactory.getLogger(WBADimensionCategory.class);

    private HashMap<String, Integer> index = new HashMap<>();
    private HashMap<String, String> label = new HashMap<>();
    private HashMap<String, String> unit = new HashMap<>();

    public Map.Entry<String, String> getFirstLabel() {

        if ( MapUtils.isEmpty(label) ) {
            LOG.debug("Label HashMap is empty.");
            return null;
        }

        return label.entrySet().iterator().next();
    }

}
