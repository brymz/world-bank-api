package com.sms.thirdparty.worldbankapi.beans.response;

import com.sms.thirdparty.worldbankapi.beans.dto.CountryIndicatorGrowth;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class GrowthResponse {

    private String indicator;
    private Integer topCountries;
    private List<Integer> dataFromYears = new ArrayList<>();
    private List<CountryIndicatorGrowth> countries = new ArrayList<>();

}
