package com.sms.thirdparty.worldbankapi.beans.dto;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

@Data
public class WBADimensionContainer {

    private static final Logger LOG = LoggerFactory.getLogger(WBADimensionContainer.class);

    private List<String> id;
    private List<Integer> size;
    private Map<String, List<String>> role;
    private WBADimension country = new WBADimension();
    private WBADimension series = new WBADimension();
    private WBADimension year = new WBADimension();

    public Integer getCountrySize() {
        return getSizeFor("country");
    }

    private Integer getSizeFor(String key) {

        if ( id.size() != size.size() ) {
            LOG.error("'id' and 'size' properties are inconsistent for '{}'.",
                      WBADimensionContainer.class.getSimpleName());
            return 0;
        }

        return size.get(id.indexOf(key));
    }

}
