package com.sms.thirdparty.worldbankapi.beans.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WBAApiResponse {

    @JsonProperty("WDI")
    private WBAJsonStat content = new WBAJsonStat();

}
