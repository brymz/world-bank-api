package com.sms.thirdparty.worldbankapi.beans.dto;

import lombok.Data;

@Data
public class WBAIndicatorConfig {

    private String alias;
    private String key;
    private Integer order;

}
