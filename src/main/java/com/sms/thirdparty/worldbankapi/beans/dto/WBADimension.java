package com.sms.thirdparty.worldbankapi.beans.dto;

import lombok.Data;

@Data
public class WBADimension {

    private String label;
    private WBADimensionCategory category = new WBADimensionCategory();

}
