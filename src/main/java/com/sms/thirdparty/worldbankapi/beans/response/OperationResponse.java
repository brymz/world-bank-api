package com.sms.thirdparty.worldbankapi.beans.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sms.thirdparty.worldbankapi.enums.ApiMessage;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OperationResponse {

    @JsonProperty("code")
    protected String code;

    @JsonProperty("message")
    protected String message;

    @JsonIgnore
    protected HttpStatus httpStatus = HttpStatus.OK;

    public OperationResponse(ApiMessage apiMessage) {
        this.code = apiMessage.getCode();
        this.message = apiMessage.getMessage();
        this.httpStatus = apiMessage.getHttpStatus();
    }

}
