package com.sms.thirdparty.worldbankapi.beans.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class WBAJsonStat {

    private String label;
    private String source;
    private LocalDate updated;
    private List<Long> value = new ArrayList<>();
    private WBADimensionContainer dimension = new WBADimensionContainer();

}
