package com.sms.thirdparty.worldbankapi.beans.dto;

import lombok.Data;

@Data
public class CountryIndicatorGrowth {

    private Integer position;
    private String isoCode;
    private String country;
    private Long totalGrowth = 0L;

}
