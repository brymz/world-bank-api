package com.sms.thirdparty.worldbankapi.controller;

import com.sms.thirdparty.worldbankapi.beans.response.OperationResponse;
import com.sms.thirdparty.worldbankapi.enums.ApiMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestController
public class GlobalExceptionController {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionController.class);

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<OperationResponse> handleAllExceptions(Exception ex, WebRequest request) {

        LOG.info("Exception raised. Generic response will be sent to client.");
        LOG.error("Exception stacktrace: ", ex);

        OperationResponse operationResponse = new OperationResponse(ApiMessage.E5XX_GENERIC_MESSAGE);
        return new ResponseEntity<>(operationResponse, operationResponse.getHttpStatus());
    }

    @ExceptionHandler({
        IllegalArgumentException.class,
        HttpRequestMethodNotSupportedException.class,
        UnsupportedOperationException.class})
    public final ResponseEntity<OperationResponse> handleNotSupportedOperation(Exception ex, WebRequest request) {

        LOG.error("Action requested is not supported", ex);

        OperationResponse operationResponse = new OperationResponse(ApiMessage.E400_GENERIC_MESSAGE);
        return new ResponseEntity<>(operationResponse, operationResponse.getHttpStatus());
    }

}
