package com.sms.thirdparty.worldbankapi.controller;

import com.sms.thirdparty.worldbankapi.beans.response.OperationResponse;
import com.sms.thirdparty.worldbankapi.enums.ApiMessage;
import com.sms.thirdparty.worldbankapi.service.WBAFetcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class WBASynchronizerController {

    @Autowired
    private WBAFetcherService fetcherService;

    @PostMapping("/sync/pull-data")
    public String requestPullData() {
        // TODO: To be implemented
        return "When this endpoint is ready, The World Bank data will be updated until year " + LocalDateTime.now().getYear() + " (if needed).";
    }

    @PostMapping("/sync/data-reset")
    public ResponseEntity<OperationResponse> requestDataReset() {
        ApiMessage apiMessage = fetcherService.executeFullDataIngest();
        return new ResponseEntity<>(new OperationResponse(apiMessage), apiMessage.getHttpStatus());
    }

}
