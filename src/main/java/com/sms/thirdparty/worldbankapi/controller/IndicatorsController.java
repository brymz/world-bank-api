package com.sms.thirdparty.worldbankapi.controller;

import com.sms.thirdparty.worldbankapi.beans.response.GrowthResponse;
import com.sms.thirdparty.worldbankapi.enums.CalculatorMethod;
import com.sms.thirdparty.worldbankapi.service.IndicatorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/indicators")
public class IndicatorsController {

    @Autowired
    private IndicatorsService indicatorsService;

    @GetMapping("/population-growth")
    public GrowthResponse getPopulationGrowthByCountry(
        @RequestParam(name = "top", defaultValue = "20", required = false) Integer top,
        @RequestParam(name = "fromYear", defaultValue = "2012", required = false) Integer fromYear,
        @RequestParam(name = "untilYear", defaultValue = "2018", required = false) Integer untilYear,
        @RequestParam(name = "calculatorMethod", defaultValue = "loop", required = false) CalculatorMethod method
    ) {
        if ( top < 1 )
            throw new IllegalArgumentException("Top countries cannot be lower than 1.");

        if ( fromYear > untilYear )
            throw new IllegalArgumentException("fromYear year cannot be greater than untilYear.");

        return indicatorsService.generatePopulationGrowthIndicator(top, fromYear, untilYear, method);
    }

    @GetMapping("/gdp-growth")
    public GrowthResponse getGDPGrowthByCountry(
            @RequestParam(name = "topGDP", defaultValue = "20", required = false) Integer topGDP,
            @RequestParam(name = "fromYear", defaultValue = "2012", required = false) Integer fromYear,
            @RequestParam(name = "untilYear", defaultValue = "2018", required = false) Integer untilYear,
            @RequestParam(name = "topPopulationGrowth", defaultValue = "0", required = false) Integer topPopulationGrowth,
            @RequestParam(name = "calculatorMethod", defaultValue = "loop", required = false) CalculatorMethod method
    ) {
        if ( topGDP < 1 )
            throw new IllegalArgumentException("Top GDP cannot be lower than 1.");

        if ( fromYear > untilYear )
            throw new IllegalArgumentException("fromYear year cannot be greater than untilYear.");

        return indicatorsService.generateGDPGrowthIndicator(
                topGDP, fromYear, untilYear, topPopulationGrowth, method);
    }

}
