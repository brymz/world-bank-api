package com.sms.thirdparty.worldbankapi.db.jdbc;

import lombok.Data;

@Data
public class GrowthByYear {

    private String indicator;
    private Long countryId;
    private String isoCode;
    private String country;
    private Integer year;
    private Integer yearPlus1;
    private Long indicator4Year;
    private Long indicator4YearPlus1;
    private Long growth;

}
