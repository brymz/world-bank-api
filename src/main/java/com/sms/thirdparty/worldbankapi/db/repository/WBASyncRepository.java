package com.sms.thirdparty.worldbankapi.db.repository;

import com.sms.thirdparty.worldbankapi.db.entity.WBASync;
import com.sms.thirdparty.worldbankapi.enums.WBASyncStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WBASyncRepository extends JpaRepository<WBASync, Long> {

    List<WBASync> findBySyncStatus(WBASyncStatus syncStatus);
    List<WBASync> findBySyncStatusAndIndicatorKeyOrderByEndsAtDesc(WBASyncStatus syncStatus, String indicatorKey);

}
