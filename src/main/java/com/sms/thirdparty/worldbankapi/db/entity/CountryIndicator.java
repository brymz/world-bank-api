package com.sms.thirdparty.worldbankapi.db.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "wba_mt_country_indicator")
public class CountryIndicator {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wba_sq_country_indicator")
    @SequenceGenerator(name = "wba_sq_country_indicator", sequenceName = "wba_sq_country_indicator", allocationSize = 1)
    private Long id;

    @Column(name = "key")
    private String key;

    @Column(name = "label")
    private String label;

    @CreationTimestamp
    @Column(name = "created_at")
    protected LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    protected LocalDateTime updatedAt;

}
