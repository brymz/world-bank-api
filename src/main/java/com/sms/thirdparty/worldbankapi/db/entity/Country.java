package com.sms.thirdparty.worldbankapi.db.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "wba_mt_country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wba_sq_country")
    @SequenceGenerator(name = "wba_sq_country", sequenceName = "wba_sq_country", allocationSize = 1)
    private Long id;

    @Column(name = "iso_code")
    private String isoCode;

    @Column(name = "name")
    private String name;

    @Column(name = "real_country")
    private Boolean realCountry;

    @CreationTimestamp
    @Column(name = "created_at")
    protected LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    protected LocalDateTime updatedAt;

}
