package com.sms.thirdparty.worldbankapi.db.jdbc;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GrowthByYearMapper implements RowMapper<GrowthByYear> {

    @Override
    public GrowthByYear mapRow(ResultSet resultSet, int i) throws SQLException {

        GrowthByYear growth = new GrowthByYear();
        growth.setIndicator(resultSet.getString("INDICATOR"));
        growth.setCountryId(resultSet.getLong("COUNTRY_ID"));
        growth.setCountry(resultSet.getString("COUNTRY"));
        growth.setIsoCode(resultSet.getString("ISO_CODE"));
        growth.setYear(resultSet.getInt("YEAR"));
        growth.setYearPlus1(resultSet.getInt("YEAR_PLUS_1"));
        growth.setIndicator4Year(resultSet.getLong("IND_4_YEAR"));
        growth.setIndicator4YearPlus1(resultSet.getLong("IND_4_YEAR_PLUS_1"));
        growth.setGrowth(resultSet.getLong("GROWTH"));

        return growth;
    }

}
