package com.sms.thirdparty.worldbankapi.db.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "wba_country_data")
public class CountryData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wba_sq_country_data")
    @SequenceGenerator(name = "wba_sq_country_data", sequenceName = "wba_sq_country_data", allocationSize = 1)
    private Long id;

    @Column(name = "year")
    private Integer year;

    @Column(name = "indicator_value")
    private Long indicatorValue;

    @ManyToOne
    @JoinColumn(name = "wba_country_id")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "wba_country_indicator_id")
    private CountryIndicator countryIndicator;

    @CreationTimestamp
    @Column(name = "created_at")
    protected LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    protected LocalDateTime updatedAt;

}
