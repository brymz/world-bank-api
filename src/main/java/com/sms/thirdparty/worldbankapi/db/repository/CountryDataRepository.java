package com.sms.thirdparty.worldbankapi.db.repository;

import com.sms.thirdparty.worldbankapi.db.entity.Country;
import com.sms.thirdparty.worldbankapi.db.entity.CountryData;
import com.sms.thirdparty.worldbankapi.db.entity.CountryIndicator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountryDataRepository extends JpaRepository<CountryData, Long> {

    Long countByCountryIndicatorAndYear(CountryIndicator indicator, Integer year);
    Optional<CountryData> findByCountryAndCountryIndicatorAndYear(Country country, CountryIndicator indicator, Integer year);
    List<CountryData> findByCountryIndicator(CountryIndicator indicator);

}
