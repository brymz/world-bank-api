package com.sms.thirdparty.worldbankapi.db.repository;

import com.sms.thirdparty.worldbankapi.db.entity.CountryIndicator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CountryIndicatorRepository extends JpaRepository<CountryIndicator, Long> {

    Optional<CountryIndicator> findByKey(String key);

}
