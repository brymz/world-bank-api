package com.sms.thirdparty.worldbankapi.db.entity;

import com.sms.thirdparty.worldbankapi.enums.WBASyncStatus;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "wba_sync_table")
public class WBASync {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wba_sq_sync_table")
    @SequenceGenerator(name = "wba_sq_sync_table", sequenceName = "wba_sq_sync_table", allocationSize = 1)
    private Long id;

    @Column(name = "indicator_key")
    private String indicatorKey;

    @Column(name = "description")
    private String description;

    @Column(name = "source_last_update")
    private LocalDate sourceLastUpdate;

    @Column(name = "sync_status")
    @Enumerated(EnumType.STRING)
    private WBASyncStatus syncStatus;

    @Column(name = "starts_at")
    private LocalDateTime startsAt;

    @Column(name = "ends_at")
    private LocalDateTime endsAt;

    @Lob
    @Column(name = "comments")
    private String comments;

}
