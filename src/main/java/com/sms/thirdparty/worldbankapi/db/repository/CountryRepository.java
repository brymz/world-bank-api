package com.sms.thirdparty.worldbankapi.db.repository;

import com.sms.thirdparty.worldbankapi.db.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    boolean existsByIsoCode(String isoCode);
    Optional<Country> findByIsoCode(String isoCode);
    List<Country> findByRealCountry(boolean realCountry);

}
