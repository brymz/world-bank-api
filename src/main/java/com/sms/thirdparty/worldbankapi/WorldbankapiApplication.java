package com.sms.thirdparty.worldbankapi;

import com.sms.thirdparty.worldbankapi.controller.GlobalExceptionController;
import com.sms.thirdparty.worldbankapi.service.WBAFetcherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorldbankapiApplication implements CommandLineRunner {

	private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionController.class);

	@Autowired
	private WBAFetcherService fetcherService;

	public static void main(String[] args) {
		SpringApplication.run(WorldbankapiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		LOG.info("Starting synchronization tasks...");
		fetcherService.clearNonFinishedProcess();
		fetcherService.retrieveAndUpdateWBAData();
		LOG.info("Synchronization tasks finished.");

	}
}
