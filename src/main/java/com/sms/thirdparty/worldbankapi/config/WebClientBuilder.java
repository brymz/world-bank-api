package com.sms.thirdparty.worldbankapi.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.channel.BootstrapHandlers;
import reactor.netty.http.client.HttpClient;

@Configuration
public class WebClientBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(WebClientBuilder.class);

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public WebClient.Builder customWebClientBuilder() {

        WebClient.Builder builder = WebClient.builder();

        if ( LOG.isDebugEnabled() ) {
            builder
                .clientConnector(new ReactorClientHttpConnector(WebClientBuilder.getHttpClientWithLogging()));
        }

        return builder;
    }

    public static HttpClient getHttpClientWithLogging() {
        return
            HttpClient
                .create()
                .tcpConfiguration(
                    tc -> tc.bootstrap(
                        b -> BootstrapHandlers.updateLogSupport(b, new HttpLoggingHandler(HttpClient.class))));
    }

}
