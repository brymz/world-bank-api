package com.sms.thirdparty.worldbankapi.config;

import com.sms.thirdparty.worldbankapi.beans.dto.WBAIndicatorConfig;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.*;

@Data
@Configuration
@ConfigurationProperties(prefix = "world-bank-api")
public class WBAConfig {

    private String baseEndpoint;
    private String populationCountryKey;
    private String gdpKey;
    private String growthByYearSql;
    private Set<Integer> yearsToSync = new HashSet<>();
    private Set<String> nonCountriesIsoCodes = new HashSet<>();
    private List<WBAIndicatorConfig> indicators = new ArrayList<>();
    private boolean resetInProgress = false;

    public List<WBAIndicatorConfig> getIndicatorsOrdered() {
        indicators.sort(Comparator.comparing(WBAIndicatorConfig::getOrder));
        return indicators;
    }

    public boolean isoCodeCountAsCountry(String isoCode) {

        if ( StringUtils.isEmpty(isoCode) )
            return false;

        return !nonCountriesIsoCodes.contains(isoCode.toUpperCase());
    }

}
