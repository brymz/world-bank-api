package com.sms.thirdparty.worldbankapi.enums;

public enum WBASyncStatus {

    SUCCESS, FAILED, IN_PROGRESS, PARTIAL_SUCCESS

}
