package com.sms.thirdparty.worldbankapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ApiMessage {

    // --------- COMMONS ERRORS ---------
    E400_GENERIC_MESSAGE(HttpStatus.BAD_REQUEST, "invalid_request", "The request performed is not supported."),
    E5XX_GENERIC_MESSAGE(HttpStatus.INTERNAL_SERVER_ERROR, "internal_server_error", "Unexpected error. Please try again."),

    // --------- OTHER MESSAGES ---------
    OK(HttpStatus.OK, "ok", "Operation successful."),
    OPERATION_SUBMITTED(HttpStatus.ACCEPTED, "ok", "Operation requested."),
    RESET_IN_PROGRESS(HttpStatus.ALREADY_REPORTED, "in_progress", "Full World Bank data reset is in progress."),
    ;

    private final HttpStatus httpStatus;
    private final String code;
    private final String message;

}
