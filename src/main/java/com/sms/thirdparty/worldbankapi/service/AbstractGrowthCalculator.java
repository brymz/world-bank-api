package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.beans.dto.CountryIndicatorGrowth;

import java.util.List;

public interface AbstractGrowthCalculator {

    List<CountryIndicatorGrowth> calculateGrowthByPeriod(
        String indicator, Integer topCountries, List<Integer> targetYears);

}
