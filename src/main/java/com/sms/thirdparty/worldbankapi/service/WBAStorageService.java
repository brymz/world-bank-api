package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.beans.dto.WBAApiResponse;
import com.sms.thirdparty.worldbankapi.beans.dto.WBADimensionCategory;
import com.sms.thirdparty.worldbankapi.beans.dto.WBADimensionContainer;
import com.sms.thirdparty.worldbankapi.beans.dto.WBAJsonStat;
import com.sms.thirdparty.worldbankapi.config.WBAConfig;
import com.sms.thirdparty.worldbankapi.db.entity.Country;
import com.sms.thirdparty.worldbankapi.db.entity.CountryData;
import com.sms.thirdparty.worldbankapi.db.entity.CountryIndicator;
import com.sms.thirdparty.worldbankapi.db.repository.CountryDataRepository;
import com.sms.thirdparty.worldbankapi.db.repository.CountryIndicatorRepository;
import com.sms.thirdparty.worldbankapi.db.repository.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class WBAStorageService {

    private static final Logger LOG = LoggerFactory.getLogger(WBAStorageService.class);

    @Autowired
    private WBAConfig wbaConfig;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CountryIndicatorRepository indicatorRepository;

    @Autowired
    private CountryDataRepository dataRepository;

    public void processAndPersist(WBAApiResponse apiResponse) throws Exception {

        if ( null == apiResponse )
            throw new IllegalArgumentException("No '" + WBAApiResponse.class.getSimpleName() + "' valid instance provided. Process skipped.");

        List<Long> dimensionValues = apiResponse.getContent().getValue();

        if ( CollectionUtils.isEmpty(dimensionValues) )
            throw new IllegalArgumentException("No values present into '" + WBAJsonStat.class.getSimpleName() + "'.");

        WBADimensionContainer dimensionContainer = apiResponse.getContent().getDimension();
        WBADimensionCategory countryCategory = dimensionContainer.getCountry().getCategory();

        if ( dimensionValues.size() != countryCategory.getIndex().size() ||
                dimensionValues.size() != countryCategory.getLabel().size() )
            throw new IllegalArgumentException("Datasets length for dimension to be processed are inconsistent.");

        updateCountries(dimensionContainer);

        Map.Entry<String, String> yearKeySet =
            dimensionContainer.getYear().getCategory().getFirstLabel();

        Map.Entry<String, String> seriesKeySet =
            dimensionContainer.getSeries().getCategory().getFirstLabel();

        String indicatorKeyName = seriesKeySet.getKey();
        Integer year = Integer.parseInt(yearKeySet.getValue());

        LOG.info("Validating indicator '{}' ...", indicatorKeyName);

        CountryIndicator countryIndicator =
            indicatorRepository
                .findByKey(indicatorKeyName)
                .orElseGet(() -> {
                    CountryIndicator ind = new CountryIndicator();
                    ind.setKey(indicatorKeyName);
                    ind.setLabel(seriesKeySet.getValue());
                    indicatorRepository.save(ind);
                    LOG.info("Country indicator key '{}' added to database.", ind.getKey());
                    return ind;
                });

        List<CountryData> countriesData = new ArrayList<>();

        Long indicatorDataLength =
            dataRepository.countByCountryIndicatorAndYear(countryIndicator, year);

        LOG.info("Processing indicator values for each country retrieved ...");

        for ( Map.Entry<String, Integer> countryIndex :
                countryCategory.getIndex().entrySet() ) {

            Optional<Country> country =
                countryRepository.findByIsoCode(countryIndex.getKey());

            if ( !country.isPresent() ) {
                LOG.warn("Country ISO Code '{}' not found in database. Indicator data skipped.",
                         countryIndex.getKey());
                continue;
            }

            CountryData countryData = 0 == indicatorDataLength ? new CountryData() :
                dataRepository
                    .findByCountryAndCountryIndicatorAndYear(country.get(), countryIndicator, year)
                    .orElse(new CountryData());
            countryData.setYear(year);
            countryData.setCountryIndicator(countryIndicator);
            countryData.setCountry(country.get());
            countryData.setIndicatorValue(dimensionValues.get(countryIndex.getValue()));

            countriesData.add(countryData);
        }

        if ( countriesData.isEmpty() ) {
            LOG.info("There is no new data to be added.");
        } else {
            dataRepository.saveAll(countriesData);
            LOG.info("{} records added in indicator '{}' for year {}.",
                     countriesData.size(), indicatorKeyName, year);
        }
    }

    private void updateCountries(WBADimensionContainer dimensionContainer) {

        LOG.info("Validating countries...");

        long countriesInDb = countryRepository.count();

        if ( countriesInDb < dimensionContainer.getCountrySize() ) {

            LOG.info("Countries retrieved from World Bank Api are greater than present in local database. Updating...");

            WBADimensionCategory countryCategory =
                dimensionContainer.getCountry().getCategory();
            List<Country> countries = new ArrayList<>();

            for ( Map.Entry<String, String> countryLabel :
                    countryCategory.getLabel().entrySet() ) {

                String isoCode = countryLabel.getKey();

                boolean shouldAdd = 0 == countriesInDb ||
                    !countryRepository.existsByIsoCode(isoCode);

                if ( !shouldAdd ) continue;

                Country country = new Country();
                country.setIsoCode(isoCode);
                country.setName(countryLabel.getValue());
                country.setRealCountry(wbaConfig.isoCodeCountAsCountry(isoCode));
                countries.add(country);
            }

            countryRepository.saveAll(countries);

            LOG.info("{} countries added to database.", countries.size());
        }
    }

    public void eraseAllTables() {
        dataRepository.deleteAllInBatch();
        indicatorRepository.deleteAllInBatch();
        countryRepository.deleteAllInBatch();
    }

}
