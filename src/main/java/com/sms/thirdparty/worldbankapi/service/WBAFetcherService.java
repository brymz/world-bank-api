package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.beans.dto.WBAApiResponse;
import com.sms.thirdparty.worldbankapi.beans.dto.WBAIndicatorConfig;
import com.sms.thirdparty.worldbankapi.config.WBAConfig;
import com.sms.thirdparty.worldbankapi.db.entity.WBASync;
import com.sms.thirdparty.worldbankapi.db.repository.WBASyncRepository;
import com.sms.thirdparty.worldbankapi.enums.ApiMessage;
import com.sms.thirdparty.worldbankapi.enums.WBASyncStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class WBAFetcherService {

    private static final Logger LOG = LoggerFactory.getLogger(WBAFetcherService.class);

    @Autowired
    private WBAConfig wbaConfig;

    @Autowired
    private WBASyncRepository syncRepository;

    @Autowired
    private WorldBankService worldBankService;

    @Autowired
    private WBAStorageService storageService;

    public void retrieveAndUpdateWBAData() {
        retrieveAndUpdateWBAData(wbaConfig.getYearsToSync());
    }

    /**
     * This method ALWAYS will go to World Bank Data Api to retrieve
     * and refresh/store data into local database.
     */
    public void retrieveAndUpdateWBAData(Set<Integer> yearsToSync) {

        List<WBAIndicatorConfig> indicatorsToSync = wbaConfig.getIndicatorsOrdered();

        for ( WBAIndicatorConfig indicator : indicatorsToSync ) {

            String indicatorKey = indicator.getKey();
            String alias = indicator.getAlias();

            LOG.info("Starting sync for indicator: {} ({}) ...", alias, indicatorKey);
            LOG.info("Validating if there is another sync process in progress...");

            if ( isAProcessInProgressFor(indicatorKey) ) {
                LOG.info("Another sync process for key '{}' is in progress. Sync omitted.", indicatorKey);
                LOG.info("--- Sync finished. ---");
                continue;
            }

            WBASync wbaSync = new WBASync();
            wbaSync.setIndicatorKey(indicatorKey);
            wbaSync.setDescription(alias);
            wbaSync.setSyncStatus(WBASyncStatus.IN_PROGRESS);
            wbaSync.setStartsAt(LocalDateTime.now());
            syncRepository.save(wbaSync);

            try {
                List<String> failedYears = new ArrayList<>();

                for ( Integer year : yearsToSync ) {

                    WBAApiResponse apiResponse =
                        worldBankService.retrieveWBDataFor(indicatorKey, year);

                    if ( null == apiResponse ) {
                        LOG.error("No data could be retrieved for indicator: '{}' | Year: '{}'. Skipped.",
                                  indicatorKey, year);
                        failedYears.add(String.valueOf(year));
                        continue;
                    }

                    LOG.info("Validating and processing data retrieved ...");

                    try {
                        storageService.processAndPersist(apiResponse);
                        wbaSync.setSourceLastUpdate(apiResponse.getContent().getUpdated());
                    } catch (Exception e) {
                        LOG.error("Error at processing api result for indicator: '{}' | Year: '{}'.",
                                  indicatorKey, year, e);
                    }
                }

                if ( failedYears.isEmpty() ) {
                    wbaSync.setSyncStatus(WBASyncStatus.SUCCESS);
                } else {
                    WBASyncStatus status =
                        failedYears.size() == yearsToSync.size() ?
                            WBASyncStatus.FAILED : WBASyncStatus.PARTIAL_SUCCESS;
                    wbaSync.setSyncStatus(status);
                    wbaSync.setComments(
                            "Following years were not synchronized: " +
                            String.join(",", failedYears) +
                            ". See logs for more detailed information.");
                }
            } catch (Exception e) {
                LOG.error("Error at handling of indicator: '{}'", indicatorKey);
                wbaSync.setSyncStatus(WBASyncStatus.FAILED);
                wbaSync.setComments("Exception message: " + e.getMessage());
            }

            wbaSync.setEndsAt(LocalDateTime.now());
            syncRepository.save(wbaSync);

            LOG.info("--- Sync finished. ---");
        }

    }

    public boolean isAProcessInProgressFor(String indicatorKey) {

        List<WBASync> wbaSyncs =
            syncRepository
                .findBySyncStatusAndIndicatorKeyOrderByEndsAtDesc(WBASyncStatus.IN_PROGRESS, indicatorKey);

        return !CollectionUtils.isEmpty(wbaSyncs);
    }

    public void clearNonFinishedProcess() {

        LOG.info("Cleaning non-finished sync processes...");

        syncRepository
            .findBySyncStatus(WBASyncStatus.IN_PROGRESS)
            .forEach(sp -> {
                sp.setSyncStatus(WBASyncStatus.FAILED);
                sp.setDescription("Process not finished as expected. Error status assigned by cleaning process.");
                sp.setEndsAt(LocalDateTime.now());
                syncRepository.save(sp);
            });

        LOG.info("Sync processes cleaned.");
    }

    public ApiMessage executeFullDataIngest() {

        if ( wbaConfig.isResetInProgress() ) {
            return ApiMessage.RESET_IN_PROGRESS;
        }

        wbaConfig.setResetInProgress(true);
        clearNonFinishedProcess();
        storageService.eraseAllTables();
        retrieveAndUpdateWBAData();
        wbaConfig.setResetInProgress(false);

        return ApiMessage.OK;
    }

}
