package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.beans.dto.CountryIndicatorGrowth;
import com.sms.thirdparty.worldbankapi.db.entity.Country;
import com.sms.thirdparty.worldbankapi.db.entity.CountryData;
import com.sms.thirdparty.worldbankapi.db.entity.CountryIndicator;
import com.sms.thirdparty.worldbankapi.db.repository.CountryDataRepository;
import com.sms.thirdparty.worldbankapi.db.repository.CountryIndicatorRepository;
import com.sms.thirdparty.worldbankapi.db.repository.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class RecursionBasedCalculatorService extends GrowthCalculatorService {

    private static final Logger LOG = LoggerFactory.getLogger(RecursionBasedCalculatorService.class);

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CountryIndicatorRepository indicatorRepository;

    @Autowired
    private CountryDataRepository countryDataRepository;

    @Override
    public List<CountryIndicatorGrowth> calculateGrowthByPeriod(
        String indicator, Integer topCountries, List<Integer> targetYears) {

        Optional<CountryIndicator> countryIndicator = indicatorRepository.findByKey(indicator);

        if ( !countryIndicator.isPresent() )
            throw new UnsupportedOperationException("Indicator '" + indicator + "' was not found.");

        List<CountryData> indicatorData =
            countryDataRepository.findByCountryIndicator(countryIndicator.get());

        if ( indicatorData.isEmpty() ) {
            LOG.info("There is no data to calculate indicator '{}'.", indicator);
            return Collections.emptyList();
        }

        List<Country> countries = countryRepository.findByRealCountry(true);

        List<CountryIndicatorGrowth> topIndicatorGrowth = new ArrayList<>();

        for ( Country country : countries ) {

            Long totalGrowth = calculateGrowthFor(indicatorData, country, targetYears, targetYears.size());

            CountryIndicatorGrowth indicatorGrowth = new CountryIndicatorGrowth();
            indicatorGrowth.setTotalGrowth(totalGrowth);
            indicatorGrowth.setCountry(country.getName());
            indicatorGrowth.setIsoCode(country.getIsoCode());
            topIndicatorGrowth.add(indicatorGrowth);
        }

        return fillPositionAndSortAndLimit(topIndicatorGrowth, topCountries);
    }

    private Long calculateGrowthFor(List<CountryData> indicatorData,
                                    Country country,
                                    List<Integer> targetYears,
                                    Integer yearIdx) {

        if ( yearIdx < 1 ) return 0L;
        int idx = yearIdx - 1;
        Integer targetYear = targetYears.get(idx);
        Long valueYearPlus1 = getIndicatorValueFor(indicatorData, country.getId(), targetYear + 1);
        Long valueYear = getIndicatorValueFor(indicatorData, country.getId(), targetYear);
        Long growthForTargetYear = valueYearPlus1 - valueYear;

        LOG.debug("Indicator growth for '{}' in {} was {}.",
                  country.getName(), targetYear, growthForTargetYear);

        return growthForTargetYear + calculateGrowthFor(indicatorData, country, targetYears, idx);
    }

    private Long getIndicatorValueFor(List<CountryData> indicatorData, Long countryId, Integer year) {
        return indicatorData
                .stream()
                .filter(id -> id.getCountry().getId().equals(countryId))
                .filter(id -> id.getYear().equals(year))
                .findFirst()
                .map(CountryData::getIndicatorValue)
                .orElse(0L);
    }

}
