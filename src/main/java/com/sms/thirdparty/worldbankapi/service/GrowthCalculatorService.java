package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.beans.dto.CountryIndicatorGrowth;
import com.sms.thirdparty.worldbankapi.config.WBAConfig;
import com.sms.thirdparty.worldbankapi.db.jdbc.GrowthByYear;
import com.sms.thirdparty.worldbankapi.db.jdbc.GrowthByYearMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

@Service
public abstract class GrowthCalculatorService implements AbstractGrowthCalculator {

    @Autowired
    protected WBAConfig wbaConfig;

    @Autowired
    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    protected List<GrowthByYear> calculateGrowthFor(String indicator, Integer year) {

        SqlParameterSource namedParameters =
            new MapSqlParameterSource()
                .addValue("year", year)
                .addValue("indicatorKey", indicator);
        return
            namedParameterJdbcTemplate
                .query(wbaConfig.getGrowthByYearSql(), namedParameters, new GrowthByYearMapper());
    }

    protected List<CountryIndicatorGrowth>
        fillPositionAndSortAndLimit(List<CountryIndicatorGrowth> countryIndicators, Integer limit) {

        countryIndicators.sort(Comparator.comparing(CountryIndicatorGrowth::getTotalGrowth));
        Collections.reverse(countryIndicators);

        IntStream
            .range(0, countryIndicators.size())
            .forEach(idx -> countryIndicators.get(idx).setPosition(idx + 1));

        return limit > countryIndicators.size() ?
                countryIndicators : countryIndicators.subList(0, limit);
    }

}
