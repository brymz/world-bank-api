package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.beans.dto.CountryIndicatorGrowth;
import com.sms.thirdparty.worldbankapi.db.jdbc.GrowthByYear;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class LoopBasedCalculatorService extends GrowthCalculatorService {

    private static final Logger LOG = LoggerFactory.getLogger(LoopBasedCalculatorService.class);

    @Override
    public List<CountryIndicatorGrowth> calculateGrowthByPeriod(
        String indicator, Integer topCountries, List<Integer> targetYears) {

        Map<String, CountryIndicatorGrowth> maxGrowthInPeriod = new HashMap<>();

        targetYears.forEach(year -> {

            List<GrowthByYear> growthByYearData = calculateGrowthFor(indicator, year);

            if ( CollectionUtils.isEmpty(growthByYearData) ) {
                LOG.info("There is no data for indicator: '{}' | year: {} ", indicator, year);
                return;
            }

            growthByYearData.forEach(record -> {

                if ( !maxGrowthInPeriod.containsKey(record.getIsoCode()) ) {

                    CountryIndicatorGrowth countryGrowth = new CountryIndicatorGrowth();
                    countryGrowth.setIsoCode(record.getIsoCode());
                    countryGrowth.setCountry(record.getCountry());
                    countryGrowth.setTotalGrowth(record.getGrowth());
                    maxGrowthInPeriod.put(record.getIsoCode(), countryGrowth);
                    return;
                }

                CountryIndicatorGrowth countryGrowth =
                    maxGrowthInPeriod.get(record.getIsoCode());
                Long growth =
                    countryGrowth.getTotalGrowth() +
                    (null == record.getGrowth() ? 0 : record.getGrowth());
                countryGrowth.setTotalGrowth(growth);
                LOG.debug("Indicator growth for '{}' in {} was {}. Sum: {}",
                    countryGrowth.getCountry(), record.getYear(), record.getGrowth(), growth);
            });
        });

        if ( MapUtils.isEmpty(maxGrowthInPeriod) || topCountries < 0 )
            return Collections.emptyList();

        List<CountryIndicatorGrowth> topIndicatorGrowth =
            new ArrayList<>(maxGrowthInPeriod.values());

        return fillPositionAndSortAndLimit(topIndicatorGrowth, topCountries);
    }

}
