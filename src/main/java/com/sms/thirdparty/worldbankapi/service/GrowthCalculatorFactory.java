package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.enums.CalculatorMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Factory Design Pattern based on Spring Context
 */
@Component
public class GrowthCalculatorFactory {

    private static final Logger LOG = LoggerFactory.getLogger(GrowthCalculatorFactory.class);

    @Autowired
    private ApplicationContext context;

    public AbstractGrowthCalculator getCalculatorService(CalculatorMethod method) {

        Class<? extends GrowthCalculatorService> clazz;

        switch (method) {
            case loop:
                clazz = LoopBasedCalculatorService.class;
                break;
            case recursion:
                clazz = RecursionBasedCalculatorService.class;
                break;
            default:
                throw new IllegalArgumentException("Calculator method not specified.");
        }

        LOG.debug("Growth Calculator selected: {}", clazz.getSimpleName());

        return context.getBean(clazz);
    }

}
