package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.beans.dto.WBAApiResponse;
import com.sms.thirdparty.worldbankapi.config.WBAConfig;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Service
public class WorldBankService {

    private static final Logger LOG = LoggerFactory.getLogger(WorldBankService.class);

    @Autowired
    private WBAConfig wbaConfig;

    @Autowired
    private WebClient.Builder webClientBuilder;

    public WBAApiResponse retrieveWBDataFor(String key, Integer year) {
        Map<String, String> queryParams = getDefaultQueryParams();
        queryParams.put("date", String.valueOf(year));
        return retrieveWBDataFor(key, queryParams);
    }

    public WBAApiResponse retrieveWBDataFor(String key, Map<String, String> queryParams) {

        String url = wbaConfig.getBaseEndpoint();
        WebClient client = webClientBuilder.baseUrl(url).build();

        Mono<WBAApiResponse> indicatorItems =
            client.get()
                .uri(uriBuilder -> {

                    uriBuilder.path("/" + key);

                    if ( MapUtils.isNotEmpty(queryParams) )
                        queryParams.forEach(uriBuilder::queryParam);

                    URI uri = uriBuilder.build();

                    LOG.info("Retrieving data from: {} ...", uri.toString());

                    return uri;
                }).exchange()
                .flatMap(response -> {
                    LOG.info("Request Status: {}", response.statusCode());
                    return response.statusCode().is2xxSuccessful() ?
                            response.bodyToMono(WBAApiResponse.class) : Mono.empty();
                }).onErrorResume(e -> {
                    LOG.error("Error at getting data from World Bank Api.", e);
                    return Mono.empty();
                });

        return indicatorItems.block();
    }

    private Map<String, String> getDefaultQueryParams() {

        Integer year =
            LocalDate.now()
                .minus(1L, ChronoUnit.YEARS)
                .getYear();

        Map<String, String> map = new HashMap<>();
        map.put("format", "jsonstat");
        map.put("date", String.valueOf(year));

        return map;
    }

}
