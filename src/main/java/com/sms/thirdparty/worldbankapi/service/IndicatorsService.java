package com.sms.thirdparty.worldbankapi.service;

import com.sms.thirdparty.worldbankapi.beans.dto.CountryIndicatorGrowth;
import com.sms.thirdparty.worldbankapi.beans.response.GrowthResponse;
import com.sms.thirdparty.worldbankapi.config.WBAConfig;
import com.sms.thirdparty.worldbankapi.enums.CalculatorMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class IndicatorsService {

    private static final Logger LOG = LoggerFactory.getLogger(IndicatorsService.class);

    @Autowired
    private WBAConfig wbaConfig;

    @Autowired
    private GrowthCalculatorFactory calculatorFactory;

    public GrowthResponse
        generatePopulationGrowthIndicator(Integer top,
                                          Integer fromYear,
                                          Integer untilYear,
                                          CalculatorMethod method) {
        return
            generateIndicatorGrowthFor(
                wbaConfig.getPopulationCountryKey(), top, fromYear, untilYear, method);
    }

    public GrowthResponse
        generateGDPGrowthIndicator(Integer topGDP,
                                   Integer fromYear,
                                   Integer untilYear,
                                   Integer topPopulationGrowth,
                                   CalculatorMethod method) {

        GrowthResponse gdpGrowth =
            generateIndicatorGrowthFor(wbaConfig.getGdpKey(), topGDP, fromYear, untilYear, method);

        if ( topPopulationGrowth > 0 ) {

            GrowthResponse populationGrowth =
                generatePopulationGrowthIndicator(topPopulationGrowth, fromYear, untilYear, method);

            List<String> validCountries =
                populationGrowth.getCountries()
                    .stream()
                    .map(CountryIndicatorGrowth::getIsoCode)
                    .collect(Collectors.toList());

            List<CountryIndicatorGrowth> filteredCountries =
                gdpGrowth
                    .getCountries()
                    .stream()
                    .filter(ci -> validCountries.contains(ci.getIsoCode()))
                    .collect(Collectors.toList());

            IntStream
                .range(0, filteredCountries.size())
                .forEach(idx -> filteredCountries.get(idx).setPosition(idx + 1));

            gdpGrowth.setCountries(filteredCountries);
        }

        return gdpGrowth;
    }

    public GrowthResponse
        generateIndicatorGrowthFor(String targetIndicator,
                                   Integer top,
                                   Integer fromYear,
                                   Integer untilYear,
                                   CalculatorMethod method) {
        List<Integer> targetYears =
            IntStream
                .rangeClosed(fromYear, untilYear)
                .boxed()
                .collect(Collectors.toList());

        GrowthResponse response = new GrowthResponse();
        response.setIndicator(targetIndicator);
        response.setDataFromYears(targetYears);
        response.setTopCountries(top);

        List<CountryIndicatorGrowth> countriesGrowth =
            calculatorFactory
                .getCalculatorService(method)
                .calculateGrowthByPeriod(targetIndicator, top, targetYears);

        if ( !countriesGrowth.isEmpty() )
            response.setCountries(countriesGrowth);

        return response;
    }

}
